﻿#include <iostream>
#include <string>

class Player
{
public:

	std::string Name = "";
	int Score = 0;

	void SetName()
	{
		std::cin >> Name;
	}
	void SetScore()
	{
		std::cin >> Score;
	}

	void GetName()
	{
		std::cout << Name;
	}
	void GetScore()
	{
		std::cout << Score;
	}

};

int main()
{
	int N = 0;

	std::cout << "Enter number of players: ";
	std::cin >> N;
	std::cout << std::endl;

	Player* Table = new Player[N];

	for (int i = 0; i < N; i++) 
	{
		std::cout << "Enter N" << (i + 1) << " player name: ";
		Table[i].SetName();

		std::cout << "Enter ";
		Table[i].GetName();
		std::cout << "'s score: ";
		Table[i].SetScore();
		std::cout << std::endl;

	}

	for (int i = 0; i < N; i++)
	{
		for (int j = N - 1; j > i; j--)
		{
			if ((Table[j].Score) > (Table[j - 1].Score))
			{
				std::swap(Table[j], Table[j - 1]);
			}
		}
	} 

	std::cout <<" SCORE TABLE" << std::endl;

	for (int i = 0; i < N; i++)
	{
		Table[i].GetName();
		std::cout << "\t";
		Table[i].GetScore();
		std::cout << "\n";

	}

}